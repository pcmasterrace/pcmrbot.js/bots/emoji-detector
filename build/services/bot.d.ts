import { Service, Context } from "moleculer";
import { Typings } from "@pcmrbotjs/core-typings";
export default class EmojiDetectorService extends Service {
    constructor(broker: any);
    /**
     * Handles comments emitted from Posts RT
     * @param {Typings.Reddit.Comment} comment - The comment emitted
     * @returns Nothing
     */
    commentEventHandler(comment: Typings.Reddit.Comment): Promise<void>;
    /**
     * Handles submissions emitted from Posts RT
     * @param {Typings.Reddit.Submission} submission - The submission emitted
     * @returns Nothing
     */
    submissionEventHandler(submission: Typings.Reddit.Submission): Promise<void>;
    /**
     * Processes the comment to see whether or not it should be removed.
     * @static
     * @function
     * @name process.comment
     * @param {Typings.Reddit.Comment} ctx - Send a full Comment object for all of the parameters.
     * @returns {object} Returns an object containing a boolean denoting if the comment was removed or not.
     */
    processComment(ctx: Context<Typings.Reddit.Comment, any>): Promise<{
        removed: boolean;
    }>;
    /**
     * Processes the submission to see whether or not it should be removed.
     * @static
     * @function
     * @name process.submission
     * @param {Typings.Reddit.Submission} ctx - Send a full Submission object for all of the parameters.
     * @returns {object} Returns an object containing a boolean denoting if the submission was removed or not.
     */
    processSubmission(ctx: Context<Typings.Reddit.Submission, any>): Promise<{
        removed: boolean;
    }>;
    /**
     * Detects emoji within a string.
     * @static
     * @function
     * @name detect.emoji
     * @param {string} text - The text to detect emoji in
     * @returns {string[]} Returns an array of detected emoji
     */
    detectEmoji(ctx: Context<{
        text: string;
    }, any>): Promise<RegExpMatchArray>;
    /**
     * Detects text faces within a string.
     * @static
     * @function
     * @name detect.textfaces
     * @param {string} text - The text to detect text faces in
     * @returns {string[]} Returns an array of detected text faces
     */
    detectTextFaces(ctx: Context<{
        text: string;
    }, any>): Promise<RegExpMatchArray>;
}
