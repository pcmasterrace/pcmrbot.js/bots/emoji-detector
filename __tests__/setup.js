module.exports = function (broker) {
    broker.createService({
        name: "api.reddit.comment",
        version: 4,
        actions: {
            remove: {
                name: "remove",
                handler: jest.fn()
            },
            distinguish: {
                name: "distinguish",
                handler: jest.fn()
            }
        }
    });
    broker.createService({
        name: "api.reddit.rt.post",
        version: 4
    });
    broker.createService({
        name: "api.reddit.submission",
        version: 4,
        actions: {
            remove: {
                name: "remove",
                handler: jest.fn()
            },
            reply: {
                name: "reply",
                async handler(ctx) {
                    return { name: "t1_dickbutt" }
                }
            }
        }
    });
    broker.createService({
        name: "api.reddit.toolbox",
        version: 4,
        actions: {
            renderMessage: {
                name: "config.render",
                handler: jest.fn().mockReturnValue("Test message")
            }
        }
    });
    broker.createService({
        name: "api.reddit.user", 
        version: 4,
        actions: {
            message: {
                name: "message",
                handler: jest.fn()
            }
        }
    });
    broker.createService({
        name: "action-log", 
        version: 4,
        actions: {
            message: {
                name: "log",
                handler: jest.fn()
            }
        }
    });
}

it("does nothing", () => {
    expect(1).toEqual(1)
})