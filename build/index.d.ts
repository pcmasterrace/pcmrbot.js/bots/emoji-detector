import { ServiceBroker } from "moleculer";
import EmojiDetectorService from "./services/bot";
export default function registerAllEmojiDetectorServices(broker: ServiceBroker): void;
export { EmojiDetectorService };
