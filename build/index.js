"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmojiDetectorService = void 0;
const tslib_1 = require("tslib");
const yn_1 = tslib_1.__importDefault(require("yn"));
const bot_1 = tslib_1.__importDefault(require("./services/bot"));
exports.EmojiDetectorService = bot_1.default;
function registerAllEmojiDetectorServices(broker) {
    if (!yn_1.default(process.env.BOT_EMOJIDETECTOR_ENABLE, { default: true }))
        return;
    broker.createService(bot_1.default);
}
exports.default = registerAllEmojiDetectorServices;
