const { ServiceBroker } = require("moleculer");
const registerAllEmojiDetectorServices = require("../build/index").default;
const registerAllTestServices = require("./setup");

const broker = new ServiceBroker({
    logLevel: "warn"
});
registerAllEmojiDetectorServices(broker);
registerAllTestServices(broker);

beforeAll(() => {
    return broker.start();
});

describe("Test individual text face detection", () => {
    it("must detect :)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":)"
        });
        expect(result).toEqual([":)"]);
    });

    it("must detect :(", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":("
        });
        expect(result).toEqual([":("]);
    });

    it("must detect ¯\\_(ツ)_/¯", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "¯\\_(ツ)_/¯"
        });
        expect(result).toEqual(["¯\\_(ツ)_/¯"]);
    });

    it("must detect ¯\\\\\\_(ツ)_/¯", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "¯\\\\\\_(ツ)_/¯"
        });
        expect(result).toEqual(["¯\\\\\\_(ツ)_/¯"]);
    });

    it("must detect ;D", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ";D"
        });
        expect(result).toEqual([";D"]);
    });

    it("must detect (:", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "(:"
        });
        expect(result).toEqual(["(:"]);
    });

    it("must detect <3", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "<3"
        });
        expect(result).toEqual(["<3"]);
    });

    it("must detect :')", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":')"
        });
        expect(result).toEqual([":')"]);
    });

    it("must detect ༼ つ ◕_◕ ༽つ", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "༼ つ ◕_◕ ༽つ"
        });
        expect(result).toEqual(["༼ つ ◕_◕ ༽つ"]);
    });

    it("must detect =)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "=)"
        });
        expect(result).toEqual(["=)"]);
    });

    it("must detect ;)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ";)"
        });
        expect(result).toEqual([";)"]);
    });

    it("must detect ;_;", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ";_;"
        });
        expect(result).toEqual([";_;"]);
    });

    it("must detect :/", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":/"
        });
        expect(result).toEqual([":/"]);
    });

    it("must detect :3", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":3"
        });
        expect(result).toEqual([":3"]);
    });

    it("must detect :D", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":D"
        });
        expect(result).toEqual([":D"]);
    });

    it("must detect :9", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":9"
        });
        expect(result).toEqual([":9"]);
    });

    it("must detect :'(", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":'("
        });
        expect(result).toEqual([":'("]);
    });

    it("must detect :P", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":P"
        });
        expect(result).toEqual([":P"]);
    });

    it("must detect ( ͡° ͜ʖ ͡°)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "( ͡° ͜ʖ ͡°)"
        });
        expect(result).toEqual(["( ͡° ͜ʖ ͡°)"]);
    });

    it("must detect :p", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":p"
        });
        expect(result).toEqual([":p"]);
    });

    it("must detect [*~*]", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "[*~*]"
        });
        expect(result).toEqual(["[*~*]"]);
    });

    it("must detect ;P", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ";P"
        });
        expect(result).toEqual([";P"]);
    });

    it("must detect ;p", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ";p"
        });
        expect(result).toEqual([";p"]);
    });

    it("must detect ☆", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "☆"
        });
        expect(result).toEqual(["☆"]);
    });

    it("must detect D:", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "D:"
        });
        expect(result).toEqual(["D:"]);
    });

    it("must detect Σ(ﾟДﾟ)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "Σ(ﾟДﾟ)"
        });
        expect(result).toEqual(["Σ(ﾟДﾟ)"]);
    });

    it("must detect 8'^)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "8'^)"
        });
        expect(result).toEqual(["8'^)"]);
    });

    it("must detect ;^)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ";^)"
        });
        expect(result).toEqual([";^)"]);
    });

    it("must detect -_-", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "-_-"
        });
        expect(result).toEqual(["-_-"]);
    });

    it("must detect :o", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":o"
        });
        expect(result).toEqual([":o"]);
    });

    it("must detect o_o", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "o_o"
        });
        expect(result).toEqual(["o_o"]);
    });

    it("must detect T_T", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "T_T"
        });
        expect(result).toEqual(["T_T"]);
    });

    it("must detect :-(", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":-("
        });
        expect(result).toEqual([":-("]);
    });

    it("must detect ᕕ( ᐛ )ᕗ", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "ᕕ( ᐛ )ᕗ"
        });
        expect(result).toEqual(["ᕕ( ᐛ )ᕗ"]);
    });

    it("must detect ._.", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "._."
        });
        expect(result).toEqual(["._."]);
    });

    it("must detect :^)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":^)"
        });
        expect(result).toEqual([":^)"]);
    });

    it("must detect O__O", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "O__O"
        });
        expect(result).toEqual(["O__O"]);
    });

    it("must detect *_*", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "*_*"
        });
        expect(result).toEqual(["*_*"]);
    });

    it("must detect :,)", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ":,)"
        });
        expect(result).toEqual([":,)"]);
    });

    it("must detect >_>", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ">_>"
        });
        expect(result).toEqual([">_>"]);
    });

    it("must detect <_<", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "<_<"
        });
        expect(result).toEqual(["<_<"]);
    });

    it("must detect x_x", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "x_x"
        });
        expect(result).toEqual(["x_x"]);
    });

    it("must detect ¯\\_(๑❛ᴗ❛๑)_/¯", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "¯\\_(๑❛ᴗ❛๑)_/¯"
        });
        expect(result).toEqual(["¯\\_(๑❛ᴗ❛๑)_/¯"]);
    });  
});

describe("Test bulk text face detection", () => {
    it("must detect 5 text faces", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "༼ つ ◕_◕ ༽つ ¯\\_(๑❛ᴗ❛๑)_/¯ D: [*~*] o_o"
        });
        expect(result.length).toEqual(5);
    });

    it("must not detect any emoji", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.textfaces", {
            text: "What the fuck did you just fucking say about me, you little bitch? I'll have you know I graduated top of my class in the Navy Seals, and I've been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You're fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little \"clever\" comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn't, you didn't, and now you're paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You're fucking dead, kiddo."
        });
        expect(result.length).toEqual(0);
    });  
});