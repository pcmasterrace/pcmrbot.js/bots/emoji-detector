const { ServiceBroker, Errors } = require("moleculer");

const registerAllEmojiDetectorServices = require("../build/index").default;
const registerAllTestServices = require("./setup");

const broker = new ServiceBroker({
    logLevel: "warn"
});
registerAllEmojiDetectorServices(broker);
registerAllTestServices(broker);

beforeAll(() => {
    return broker.start();
});

it("should remove this post", async () => {
    let comment = {};
    Object.assign(comment, testComment, {body: ":) :) :) :)"});
    const results = await broker.call("v4.bot.emoji-detector.process.comment", comment);
    expect(results).toEqual({removed: true});
});

it("should not remove this post", async () => {
    let comment = {};
    Object.assign(comment, testComment, {body: ":) :) :)"});
    const results = await broker.call("v4.bot.emoji-detector.process.comment", comment);
    expect(results).toEqual({removed: false});
});

const testComment = {
    approved: false,
    approvedAt: null,
    approvedBy: null,
    archived: false,
    author: {
        flair: { cssClass: null, templateId: null, text: null },
        name: 't2_dickbutt',
        isSubmitter: true,
        username: 'TestAccountPleaseIgnore'
    },
    awards: [],
    body: '',
    created: new Date(),
    distinguished: false,
    edited: false,
    locked: false,
    modNote: { author: null, note: null, title: null },
    name: 't1_dickbutt',
    nsfw: false,
    parentName: 't1_dickbutt',
    parentSubmission: {
        author: 'TestAccountPleaseIgnore',
        comments: 1,
        name: 't3_dickbutt',
        permalink: 'https://www.reddit.com/r/test/comments/dickbutt/test_post_please_ignore',
        title: 'Test post please ignore',
        url: 'https://example.com'
    },
    permalink: '/r/test/comments/dickbutt/test_post_please_ignore/dickbutt',
    removalReason: null,
    removed: false,
    removedAt: null,
    removedBy: null,
    reports: { count: 0, dismissed: [], mod: [], user: [] },
    replies: [],
    score: 1,
    scoreHidden: false,
    spam: false,
    stickied: false,
    subreddit: 'test'
}