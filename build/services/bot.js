"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moleculer_1 = require("moleculer");
const es2015_1 = tslib_1.__importDefault(require("emoji-regex/es2015"));
const core_typings_1 = require("@pcmrbotjs/core-typings");
class EmojiDetectorService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "bot.emoji-detector",
            version: 4,
            dependencies: [
                { name: "action-log", version: 4 },
                { name: "api.reddit.comment", version: 4 },
                { name: "api.reddit.rt.post", version: 4 },
                { name: "api.reddit.submission", version: 4 },
                { name: "api.reddit.toolbox", version: 4 },
                { name: "api.reddit.user", version: 4 }
            ],
            settings: {
                threshold: {
                    submission: {
                        title: Number(process.env.BOT_EMOJIDETECTOR_THRESHOLD_SUBMISSION_TITLE) || 0,
                        body: Number(process.env.BOT_EMOJIDETECTOR_THRESHOLD_SUBMISSION_BODY) || 3,
                    },
                    comment: Number(process.env.BOT_EMOJIDETECTOR_THRESHOLD_COMMENT) || 3
                }
            },
            events: {
                "v4.api.reddit.rt.post.comment": this.commentEventHandler,
                "v4.api.reddit.rt.post.submission": this.submissionEventHandler
            },
            actions: {
                processComment: {
                    name: "process.comment",
                    params: core_typings_1.FastestValidator.Reddit.Comment,
                    handler: this.processComment
                },
                processSubmission: {
                    name: "process.submission",
                    params: core_typings_1.FastestValidator.Reddit.Submission,
                    handler: this.processSubmission
                },
                detectEmoji: {
                    name: "detect.emoji",
                    params: {
                        text: "string"
                    },
                    handler: this.detectEmoji
                },
                detectTextFaces: {
                    name: "detect.textfaces",
                    params: {
                        text: "string"
                    },
                    handler: this.detectTextFaces
                }
            }
        });
    }
    /**
     * Handles comments emitted from Posts RT
     * @param {Typings.Reddit.Comment} comment - The comment emitted
     * @returns Nothing
     */
    async commentEventHandler(comment) {
        await this.broker.call("v4.bot.emoji-detector.process.comment", comment);
    }
    /**
     * Handles submissions emitted from Posts RT
     * @param {Typings.Reddit.Submission} submission - The submission emitted
     * @returns Nothing
     */
    async submissionEventHandler(submission) {
        await this.broker.call("v4.bot.emoji-detector.process.submission", submission);
    }
    /**
     * Processes the comment to see whether or not it should be removed.
     * @static
     * @function
     * @name process.comment
     * @param {Typings.Reddit.Comment} ctx - Send a full Comment object for all of the parameters.
     * @returns {object} Returns an object containing a boolean denoting if the comment was removed or not.
     */
    async processComment(ctx) {
        const emojis = await ctx.call("v4.bot.emoji-detector.detect.emoji", {
            text: ctx.params.body
        });
        const textfaces = await ctx.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ctx.params.body
        });
        if (emojis.length + textfaces.length > this.settings.threshold.comment) {
            // Remove the post
            await ctx.call("v4.api.reddit.comment.remove", {
                postName: ctx.params.name
            });
            // Generate a removal message
            const message = await ctx.call("v4.api.reddit.toolbox.config.render", {
                subreddit: ctx.params.subreddit,
                useHeader: false,
                useFooter: false,
                customReason: `Hello, /u/{author}!\n\n` +
                    `Your comment was automatically removed because there are too many emojis or emoticons in the comment! Our current rules don't allow it.\n\n` +
                    `Please repost without the emojis/emoticons!\n\n` +
                    `Thank you for being part of the PCMR!`,
                author: ctx.params.author.username,
                kind: "comment",
                mod: "PCMRBot.js",
                title: ctx.params.parentSubmission.title,
                url: ctx.params.permalink
            });
            // Message the user with the thing
            await ctx.call("v4.api.reddit.user.message", {
                to: ctx.params.author.username,
                subject: `/r/${ctx.params.subreddit}`,
                text: message
            });
            // Log our ultimate victory of all time to the action logger
            await ctx.call("v4.action-log.log", {
                postName: ctx.params.name,
                service: `${this.version}.${this.name}`,
                actionTaken: "removed",
                timestamp: new Date(),
                logMessage: "Removed comment by <https://www.reddit.com/u/{author}|/u/{author}> for having too many emoji"
            });
            return {
                removed: true
            };
        }
        else {
            return {
                removed: false
            };
        }
    }
    /**
     * Processes the submission to see whether or not it should be removed.
     * @static
     * @function
     * @name process.submission
     * @param {Typings.Reddit.Submission} ctx - Send a full Submission object for all of the parameters.
     * @returns {object} Returns an object containing a boolean denoting if the submission was removed or not.
     */
    async processSubmission(ctx) {
        const emojisBody = await ctx.call("v4.bot.emoji-detector.detect.emoji", {
            text: ctx.params.body
        });
        const textfacesBody = await ctx.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ctx.params.body
        });
        const emojisTitle = await ctx.call("v4.bot.emoji-detector.detect.emoji", {
            text: ctx.params.title
        });
        const textfacesTitle = await ctx.call("v4.bot.emoji-detector.detect.textfaces", {
            text: ctx.params.title
        });
        if (emojisTitle.length + textfacesTitle.length > this.settings.threshold.submission.title ||
            emojisBody.length + textfacesBody.length > this.settings.threshold.submission.body) {
            // Remove the post
            await ctx.call("v4.api.reddit.submission.remove", {
                postName: ctx.params.name
            });
            // Generate a removal message
            const message = await ctx.call("v4.api.reddit.toolbox.config.render", {
                subreddit: ctx.params.subreddit,
                useHeader: false,
                useFooter: false,
                customReason: `Hello, /u/{author}!\n\n` +
                    `Your post was automatically removed because there are emojis or emoticons in the title! Our current rules don't allow it.\n\n` +
                    `Please repost without the emojis/emoticons!\n\n` +
                    `Thank you for being part of the PCMR!`,
                author: ctx.params.author.username,
                kind: "submission",
                mod: "PCMRBot.js",
                title: ctx.params.title,
                url: ctx.params.permalink
            });
            // Reply to the post
            const post = await ctx.call("v4.api.reddit.submission.reply", {
                postName: ctx.params.name,
                body: message
            });
            // Distinguish the comment
            await ctx.call("v4.api.reddit.comment.distinguish", {
                distinguish: true,
                postName: post.name
            });
            // Log our ultimate victory of all time to the action logger
            await ctx.call("v4.action-log.log", {
                postName: ctx.params.name,
                service: `${this.version}.${this.name}`,
                actionTaken: "removed",
                timestamp: new Date(),
                logMessage: "Removed submission by <https://www.reddit.com/u/{author}|/u/{author}> for having too many emoji"
            });
            return {
                removed: true
            };
        }
        else {
            return {
                removed: false
            };
        }
    }
    /**
     * Detects emoji within a string.
     * @static
     * @function
     * @name detect.emoji
     * @param {string} text - The text to detect emoji in
     * @returns {string[]} Returns an array of detected emoji
     */
    async detectEmoji(ctx) {
        const emoji = es2015_1.default();
        return ctx.params.text.match(emoji) || [];
    }
    /**
     * Detects text faces within a string.
     * @static
     * @function
     * @name detect.textfaces
     * @param {string} text - The text to detect text faces in
     * @returns {string[]} Returns an array of detected text faces
     */
    async detectTextFaces(ctx) {
        // https://regex101.com/r/05pExM/3
        // const textfaces = /(?:(?<=^)|(?<=\s))(?:[:;=]['^,-]?[\)\(D\/\|39PO]|¯(?:\\\\)?\\_\(ツ\)_\/¯|[\(\)D]:|<3|༼ つ ◕_◕ ༽つ|;_;|\( ͡° ͜ʖ ͡°\)|\[\*~\*\]|☆|Σ\(ﾟДﾟ\)|8'\^\)|-_-|[o*><]_+?[o*><]|T_T|\._\.|x_x|¯(?:\\\\)?\\_\(๑❛ᴗ❛๑\)_\/¯|ᕕ\( ᐛ \)ᕗ)(?:(?=\s)|(?=$))/gi
        const textfaces = /(?:^|\s)(?:[:;=]['^,-]?[\)\(D\/\|39PO]|¯(?:\\\\)?\\_\(ツ\)_\/¯|[\(\)]:|<3|༼ つ ◕_◕ ༽つ|;_;|\( ͡° ͜ʖ ͡°\)|\[\*~\*\]|☆|Σ\(ﾟДﾟ\)|8'\^\)|-_-|[o*><]_+?[o*><]|T_T|\._\.|x_x|¯(?:\\\\)?\\_\(๑❛ᴗ❛๑\)_\/¯|ᕕ\( ᐛ \)ᕗ)(?:(?=\s)|(?=$))/gi;
        return ctx.params.text.match(textfaces) || [];
    }
}
exports.default = EmojiDetectorService;
