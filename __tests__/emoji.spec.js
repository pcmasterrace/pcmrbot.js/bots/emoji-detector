const { ServiceBroker } = require("moleculer");
const registerAllEmojiDetectorServices = require("../build/index").default;
const registerAllTestServices = require("./setup");

const broker = new ServiceBroker({
    logLevel: "warn"
});
registerAllEmojiDetectorServices(broker);
registerAllTestServices(broker);

beforeAll(() => {
    return broker.start();
});

describe("Test emoji detection", () => {
    it("must detect an emoji", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.emoji", {
            text: "🤣"
        });
        expect(result).toEqual(["🤣"]);
    });
    
    it("must detect all 3 emoji", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.emoji", {
            text: "😂😍😘"
        });
        expect(result).toEqual(["😂", "😍", "😘"]);
    });

    it("must detect all 5 emoji", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.emoji", {
            text: "🤣💖😒👀💩"
        });
        expect(result).toEqual(["🤣", "💖", "😒", "👀", "💩"]);
    });
    
    it("must not detect an emoji", async () => {
        const result = await broker.call("v4.bot.emoji-detector.detect.emoji", {
            text: "there's no emoji in this"
        });
        expect(result).toEqual([]);
    });
});