const { ServiceBroker } = require("moleculer");

const registerAllEmojiDetectorServices = require("../build/index").default;
const registerAllTestServices = require("./setup");

const broker = new ServiceBroker({
    logLevel: "warn"
});
registerAllEmojiDetectorServices(broker);
registerAllTestServices(broker);

const removePost = jest.fn();
broker.createService({
    name: "api.reddit.post",
    version: 4,
    actions: {
        remove: {
            name: "remove",
            params: {
                postName: "string"
            },
            handler: removePost
        }
    }
});
broker.createService({
    name: "api.reddit.rt.post",
    version: 4
});

beforeAll(() => {
    return broker.start();
});

beforeEach(() => {
    jest.resetAllMocks();
})

describe("Test only body detection", () => {
    it("should remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {body: ":) :) :) :)"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: true});
    });

    it("should remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {body: "🤣🤣🤣🤣"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: true});
    });

    it("should not remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {body: ":) :) :)"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: false});
    });

    it("should not remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {body: "😂😍😘"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: false});
    });

    it("should remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {body: "😘 :) 🐱‍🐉 D:"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: true});
    });
});

describe("Test only title detection", () => {
    it("should remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {title: ":)"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: true});
    });

    it("should remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {title: "🤣"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: true});
    });

    it("should remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {title: "🤣 :D"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: true});
    });

    it("should not remove this post", async () => {
        let submission = {};
        Object.assign(submission, testSubmission, {title: "Just a normal title"});
        const results = await broker.call("v4.bot.emoji-detector.process.submission", submission);
        expect(results).toEqual({removed: false});
    });
});

const testSubmission = {
    approved: false,
    approvedBy: null,
    approvedAt: null,
    archived: false,
    author: {
        flair: { cssClass: null, templateId: null, text: null },
        name: 't2_dickbutt',
        username: 'TestAccountPleaseIgnore'
    },
    awards: [],
    body: "",
    commentCount: 0,
    comments: [],
    contestMode: false,
    created: new Date(),
    distinguished: false,
    domain: 'self.test',
    edited: false,
    flair: {
        cssClass: null,
        templateId: null,
        text: null
    },
    gilded: 0,
    isSelf: true,
    isVideo: false,
    locked: false,
    media: null,
    modNote: { author: null, note: null, title: null },
    name: 't3_dickbutt',
    nsfw: false,
    permalink: '/r/test/comments/dickbutt/test_post_please_ignore',
    pinned: false,
    removalReason: null,
    removed: false,
    removedAt: null,
    removedBy: null,
    reports: { count: 0, dismissed: [], mod: [], user: [] },
    score: 1,
    spam: false,
    spoiler: false,
    stickied: false,
    subreddit: 'test',
    thumbnail: 'self',
    title: 'Test post please ignore',
    url: 'https://www.reddit.com/r/test/comments/dickbutt/test_post_please_ignore',
    viewCount: 0
}