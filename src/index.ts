import { ServiceBroker } from "moleculer";
import yn from "yn";

import EmojiDetectorService from "./services/bot"

export default function registerAllEmojiDetectorServices(broker: ServiceBroker): void {
    if(!yn(process.env.BOT_EMOJIDETECTOR_ENABLE, {default: true})) return;

    broker.createService(EmojiDetectorService);
}

export {
    EmojiDetectorService
}